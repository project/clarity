<?php
  $gridsize = 48;
  $left = 12;
  $right = 12;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <script type="text/javascript"><?php /* Needed to avoid flash of unstyled content in IE */ ?></script>
</head>
<body class="<?php print $body_classes; ?>">
  <div class="page container-48 clear-block">
    
    <div class="page-branding grid-48">
      <?php if($logo): ?>
        <div class="site-logo grid-5 alpha">
          <a href="/" title="<?php print t('Home') ?>"><img src="<?php print $logo; ?>" /></a>
        </div>
      <?php endif; ?>
      
      <?php if($site_name): ?>
        <div class="site-title grid-43 omega">
          <h1 class="title"><a href="/" title="<?php print t('Home') ?>"><?php print $site_name; ?></a></h1>
        </div>
      <?php endif; ?>
      
      <?php if ($site_slogan): ?>
        <div class="site-slogan grid-43 omega">
          <?php print $site_slogan; ?>
        </div>
      <?php endif; ?>
      
      <?php if($mission): ?>
        <div class="site-mission grid-43 omega">
          <?php print $mission; ?>
        </div>
      <?php endif; ?>
      
      <?php if ($header): ?>
        <div class="page-header region grid-48 alpha omega">
          <?php print $header; ?>
        </div>
      <?php endif; ?>
    </div>
        
    <?php if ($content): ?>
      <div class="<?php print ns('grid-48', $aside_left, 9, $aside_right, 13) . ' ' . ns('push-9', !$aside_left, 9); ?>">
        <?php if (!empty($messages)): ?>
          <div class="console">
            <?php print $messages; ?>
          </div>
        <?php endif; ?>
        
        <?php if ($tabs): ?>
          <?php print $tabs; ?>
        <?php endif; ?>
        
        <?php print $content; ?>
      </div>
    <?php endif; ?>
    
    <?php if ($aside_left): ?>
      <div class="aside-left region grid-9 <?php print ns('pull-39', $aside_right, 13); ?>">
        <?php print $aside_left; ?>
      </div>
    <?php endif; ?>
    
    <?php if ($aside_right): ?>
      <div class="aside-right region grid-13">
        <?php print $aside_right; ?>
      </div>
    <?php endif; ?>
    
    <?php if ($footer || $footer_message): ?>
      <div class="page-footer region grid-48">
        <?php print $footer; ?>
        <?php if ($footer_message): ?>
          <div class="footer-message grid-48 alpha omega">
            <?php print $footer_message; ?>
          </div>
        <?php endif; ?>
      </div>
    <?php endif; ?>    

  </div>
  <?php print $scripts; ?>
  <?php print $closure; ?>
</body>
</html>
